/*
 * SPDX-License-Identifier: MIT
 *
 * Copyright © 2021 Intel Corporation
 */

#ifndef _XE_PCI_H_
#define _XE_PCI_H_

int i915_register_pci_driver(void);
void i915_unregister_pci_driver(void);

#endif /* _XE_PCI_H_ */
